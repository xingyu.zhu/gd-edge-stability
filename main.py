import numpy as np
import torch
import torch.nn as nn
from utils import FakeDL, eigenspace_stability
import algos.pyhessian as pyhessian 

import matplotlib.pyplot as plt
from phicomp import comp_phi

device = "cpu"

def get_dataset(n, d):
    # assert n >= d
    X = np.sqrt(n) * torch.qr(torch.normal(torch.zeros(n, d)))[0]

    # Can be changed to sqrt(d) * I as suggested
    # A = torch.normal(torch.zeros(d, d))
    # A = torch.eye(d) * np.sqrt(d)
    A = torch.eye(d)
    Y = torch.matmul(X, A)
    return X, Y


class LinearNet(nn.Module):
    def __init__(self, L, d):
        super(LinearNet, self).__init__()
        self.L = L
        self.layers = []
        for i in range(L):
            # For dimension debugging
            # if i == L - 1:
            #     self.layers.append(nn.Linear(d + i * 1, d, bias=False))
            # else:
            #     self.layers.append(nn.Linear(d + i * 1, d + (i + 1) * 1, bias=False))
            self.layers.append(nn.Linear(d, d, bias=False))
            self.add_module("W{}".format(i+1), self.layers[-1])

    def forward(self, x):
        for i in range(self.L):
            x = self.layers[i](x)
        return x


def compute_eigeninfo(net, dl, topn, criterion):
    # Computing the top n eigenvectors using Power Iteration Method.
    net.eval()
    hessian_comp = pyhessian.hessian(net, criterion, dataloader=dl, cuda=False if device=="cpu" else True)
    eigenvals, eigenvecs = hessian_comp.eigenvalues(top_n=topn, tol=1e-4, maxIter=1000)
    # print(eigenvals)
    net.train()
    return eigenvals, eigenvecs

def quadratic_loss(Y_hat, Y):
    """Batched quadratic loss"""
    diff = torch.square(Y - Y_hat)
    loss = diff.sum() * 0.5
    if len(Y_hat.shape) != 1:
        loss /= len(Y)
    return loss

def train(X, Y, L, d, eta, epochs, hessian_eval_epochs, hessian_topn, save_eigenvecs=True):
    """
    X, Y : dataset
    L, d : as defined in the paper
    epochs : total number of epochs to train
    hessian_eval_epochs : list of integers indicating on which epochs to calculate the eigeninformation
    hessian_topn : number of top eigenvector/values to compute
    save_eigenvecs : whether to save the eigenvectors
    """

    # Dataloader for Hessian Calculation
    dataloader = FakeDL(X, Y, device)
    loss_record = []
    eigenvals_record = []
    eigenvecs_record = []

    net = LinearNet(L, d).to(device)
    # Xavier Initialization
    # for param in net.parameters():
    #     param.data = torch.normal(torch.zeros_like(param), 1/np.sqrt(d)) # pylint: disable=no-member
    for layer in net.layers:
        nn.init.xavier_normal_(layer.weight)
    
    # criterion = nn.MSELoss(reduction='mean')
    optimizer = torch.optim.SGD(net.parameters(), lr=eta)

    for epoch in range(epochs):  # loop over the dataset multiple times
        optimizer.zero_grad()
        
        Y_hat = net(X)
        # loss = criterion(Y_hat, Y)
        loss = quadratic_loss(Y_hat, Y)
        # exit()
        
        loss.backward()
        optimizer.step()
        
        loss_record.append(loss.item())

        if epoch in hessian_eval_epochs:
            eigenvals, eigenvecs = compute_eigeninfo(net, dataloader, hessian_topn, quadratic_loss)
            eigenvals_record.append(eigenvals)
            if save_eigenvecs:
                eigenvecs_record.append(eigenvecs)

            Ws = [W.weight.data for W in net.layers]
            W_prod = Ws[0].clone()
            for W in Ws[1:]:
                W_prod = W.matmul(W_prod)

            # Alternative loss computation (squared fro norm of difference between weight and A)
            loss_alter = (W_prod - torch.eye(d) * np.sqrt(d)).square().sum() / 2

            # Phi and flattest hessian availble here
            Phi = comp_phi(Ws)
            # H_flattest_min = Phi.matmul(Phi.T) # (since we are using the loss function from 2103.00065 with a half)
            # print(H_flattest_min.shape)

        if epoch % 100 == 0:
            print("Epoch {} \t Loss: {:.6g} \t Sharpness: {:.6g}".format(epoch, loss.item(), eigenvals_record[-1][0] if len(eigenvals_record) > 0 else -1))

    return loss_record, eigenvals_record, eigenvecs_record


def visualize(loss_record, hessian_eval_epochs, eigenvals_record, eigenvecs_record, eta, d, n, L):

    title = "d{}_n{}_L{}".format(d, n, L)
    # Plot the empirical loss
    plt.figure(figsize=(12, 6))
    plt.suptitle(title)
    plt.subplot(121)
    plt.title(r"Training Loss $\eta$={:.4g}".format(eta))
    plt.plot(np.arange(len(loss_record)), loss_record, label=r'$\eta$={:.4g}'.format(eta))
    plt.xlabel('Epochs')

    # Plot the eigenvalues
    plt.subplot(122)
    plt.axhline(y=2/eta, linestyle=':')
    plt.title(r"Top eigenvalues $\eta$={:.4g}".format(eta))
    for k in range(len(eigenvals_record[0])):
        plt.scatter(hessian_eval_epochs, [v[k] for v in eigenvals_record], label=r'$\lambda_{}$'.format(k+1))
    plt.legend()
    plt.xlabel('Epochs')
    plt.savefig('./figs/{}_eta{:.4g}_epochs{}_eigenvals.jpg'.format(title, eta, len(loss_record)))

    if len(eigenvecs_record) != 0:
        eigenspace_ovlp = eigenspace_stability(eigenvecs_record)
        plt.figure(figsize=(8, 6))
        plt.suptitle(title)
        plt.title(r"Eigenspace Overlap $\eta$={:.4g} Sample rate {}".format(eta, len(loss_record) / len(eigenvals_record)))
        for k in range(len(eigenspace_ovlp)):
            plt.scatter(hessian_eval_epochs, eigenspace_ovlp[k], label='top {} eigenspace'.format(k+1))
        plt.xlabel('Epochs')
        plt.legend()
        plt.savefig('./figs/{}_eta{:.4g}_epochs{}_eigenvecs.jpg'.format(title, eta, len(loss_record)))


def main():
    # Configs, current set as proposed in the paper
    d = 50 # layer width
    n = 50 # # of samples
    L = 20 # # of layers

    X, Y = get_dataset(n, d)
    X, Y = X.to(device), Y.to(device)

    eta = 2/1000
    epochs = 30000
    hessian_eval_epochs = list(range(0, epochs, 200))

    train_result = train(X, Y, L, d, eta, epochs, hessian_eval_epochs, hessian_topn=2, save_eigenvecs=True)
    loss_record, eigenvals_record, eigenvecs_record = train_result

    visualize(loss_record, hessian_eval_epochs, eigenvals_record, eigenvecs_record, eta, d, n, L)

if __name__ == "__main__":
    main()
