### The basic code is just main.py and utils.py, the other folders are for some other direction of exploration.

Execute the code by running `python3 main.py`

Configurations and running settings (including how many top eigenvalues/vectors to compute) can be modified in the main function
```py
# Configs, current set as proposed in the paper
d = 50
n = 50
L = 20

eta = 2/1000
epochs = 30000
hessian_eval_epochs = list(range(0, epochs, 200)) # For which epochs compute the eigenthings

train_result = train(X, Y, L, d, eta, epochs, hessian_eval_epochs, hessian_topn=2, save_eigenvecs=True)
```

The script for computing the Phi term as in https://arxiv.org/pdf/2002.04710.pdf  and the alternative way of computing the quadratic objective assumpting whitened input is available after line 111 of `main.py`.
```py
# Alternative loss computation (squared fro norm of difference between weight and A)
loss_alter = (W_prod - torch.eye(d) * np.sqrt(d)).square().sum() / 2

# Phi and flattest hessian availble here
Phi = comp_phi(Ws)
H_flattest_min = Phi.matmul(Phi.T) # (since we are using the loss function from 2103.00065 with a half)
```
Since the current version of code divides the normal quadratic loss by 2 following https://arxiv.org/pdf/2103.00065.pdf, the hessian at flattest minima should just be \Phi\Phi^T.

The result will be plotted in the `./figs/` directory

Including the figure as in the original paper
![alt text](https://gitlab.cs.duke.edu/xingyu.zhu/gd-edge-stability/-/raw/master/figs/d50_n50_L20_eta0.002_epochs30000_eigenvals.jpg)

and the overlap of top eigenspaces at different sample points
![alt text](https://gitlab.cs.duke.edu/xingyu.zhu/gd-edge-stability/-/raw/master/figs/d50_n50_L20_eta0.002_epochs30000_eigenvecs.jpg)
