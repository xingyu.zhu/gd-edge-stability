import numpy as np
import torch
import torch.nn as nn
from networks import LinearNet, LinearShrinkNet
from utils import *
import algos.pyhessian as pyhessian 
import torch.nn.functional as F

from gterm_comp import g_term_net

from vis import *

device = 'cpu' if not torch.cuda.is_available() else 'cuda'

def get_dataset(n, d):
    assert n >= d
    X = np.sqrt (n) * torch.qr(torch.normal(torch.zeros(n, d)))[0] # pylint: disable=no-member
    A = torch.normal(torch.zeros(d, d)) # pylint: disable=no-member
    # A = torch.eye(d) # pylint: disable=no-member
    Y = torch.matmul(X, A) # pylint: disable=no-member
    return X, Y

def compute_eigeninfo(net, dl, topn, criterion):
    # Computing the top n eigenvectors using Power Iteration Method.
    net.eval()
    hessian_comp = pyhessian.hessian(net, criterion, dataloader=dl, cuda=True)
    eigenvals, eigenvecs = hessian_comp.eigenvalues(top_n=topn, tol=1e-6, maxIter=1000)
    # print(eigenvals)
    net.train()
    return eigenvals, eigenvecs

def g_term_eigeninfo(net, dl, topn, niter=10):
    G_mean, F_mean, Gdiag_mean = g_term_net(net, dl)
    UG, SG, DG = torch.svd_lowrank(G_mean, q=10, niter=niter)
    UGdiag, SGdiag, DGdiag = torch.svd_lowrank(Gdiag_mean, q=10, niter=niter)
    G_eig, Gdiag_eig = UG.T[0], UGdiag.T[0]
    print(G_eig.dot(Gdiag_eig), SG[:3])

def train(X, Y, L, d, eta, epochs, hessian_eval_epochs, hessian_topn, save_eigenvecs=True):
    print("d{}_e{}_L{}_topn{}".format(d, epochs, L, hessian_topn))

    # Dataloader for Hessian Calculation
    net = LinearNet(L, d)
    # net = LinearShrinkNet(L, d)

    net, device = prepare_net(net)
    dataloader = FakeDL(X, Y, device)
    loss_record = []
    eigenvals_record = []
    eigenvecs_record = []

    # Xavier Initialization
    for param in net.parameters():
        param.data = torch.normal(torch.zeros_like(param), 1/np.sqrt(d)) # pylint: disable=no-member

    criterion = nn.MSELoss()
    optimizer = torch.optim.SGD(net.parameters(), lr=eta)
    init_net_vec = None

    # exit()

    for epoch in range(epochs):  # loop over the dataset multiple times
        optimizer.zero_grad()

        Y_hat = net(X)
        loss = criterion(Y_hat, Y)
        
        loss.backward()
        optimizer.step()


        if epoch in hessian_eval_epochs:

            # g_eigenvals = g_term_eigeninfo(net, dataloader, hessian_topn)
            eigenvals, eigenvecs = compute_eigeninfo(net, dataloader, hessian_topn, criterion)
            eigenvals_record.append(eigenvals)
            print(eigenvals)
            if save_eigenvecs:
                eigenvecs_record.append(eigenvecs)
            loss_record.append(loss.item())
            

        if epoch % 500 == 0 or epoch in hessian_eval_epochs:
            print("Epoch {} \t Loss: {:.6g} \t Sharpness: {:.6g}".format(epoch, loss.item(), eigenvals_record[-1][0] if len(eigenvals_record) > 0 else -1))
    
    misc = []

    return loss_record, eigenvals_record, eigenvecs_record, misc

def main():
    d = 20
    n = 50
    L = 20
    torch.random.manual_seed(0)

    X, Y = get_dataset(n, d)
    X, Y = X.to(device), Y.to(device)

    eta = 2/1000
    epochs = 20000
    # hessian_eval_epochs = list(range(15500, 16500, 1))
    hessian_eval_epochs = list(range(18500, 20000, 1))
    # hessian_eval_epochs = list(range(0,10000,1000))

    train_result = train(X, Y, L, d, eta, epochs, hessian_eval_epochs, hessian_topn=1, save_eigenvecs=False)
    loss_record, eigenvals_record, eigenvecs_record, misc = train_result

    visualize_base(loss_record, hessian_eval_epochs, eigenvals_record, eigenvecs_record, eta, d, n, L)

if __name__ == "__main__":
    main()
