import torch
import numpy as np
import functools
from copy import deepcopy
from collections import OrderedDict

def rgetattr(obj, attr, *args):
    def _getattr(obj, attr):
        return getattr(obj, attr, *args)
    return functools.reduce(_getattr, [obj] + attr.split('.'))

def bkp_2d(t1, t2):
    btsize, t1_h, t1_w = t1.size()
    btsize, t2_h, t2_w = t2.size()
    out_h = t1_h * t2_h
    out_w = t1_w * t2_w

    expanded_t1 = (
        t1.unsqueeze(3)
          .unsqueeze(4)
          .repeat(1, 1, t2_h, t2_w, 1)
          .view(btsize, out_h, out_w)
    )
    for i in range(t1_h):
        for j in range(t1_w):
            expanded_t1[:, i * t2_h: (i + 1) * t2_h, j * t2_w: (j + 1) * t2_w] *= t2
    return expanded_t1

class IntermediateFeatureExtractor:

    def __init__(self, net, target_layers, evaluate=True):
        self.net = net
        self.evaluate = evaluate
        self.target_layers = target_layers
        
    def __call__(self, *args, **kwargs):
        if self.evaluate:
            self.net.eval()
        ret, handles = {}, []
        for var_name in self.target_layers:
            try:
                layer = rgetattr(self.net, var_name)
            except:
                print("var name not found")
            def hook(module, feature_in, feature_out, name=var_name):
                assert name not in ret
                if isinstance(feature_in, tuple):
                    feature_in = feature_in[0]
                if isinstance(feature_out, tuple):
                    feature_out = feature_out[0]
                ret[name] = [feature_in.detach(), feature_out.detach()]
            h = layer.register_forward_hook(hook)
            handles.append(h)
            
        output = self.net(*args, **kwargs).detach()
            
        for h in handles:
            h.remove()
            
        if self.evaluate:
            self.net.train()
        
        return ret, output

def matrix_diag(diagonal):
    N = diagonal.shape[-1]
    shape = diagonal.shape[:-1] + (N, N)
    device, dtype = diagonal.device, diagonal.dtype
    result = torch.zeros(shape, dtype=dtype, device=device) # pylint: disable=no-member
    indices = torch.arange(result.numel(), device=device).reshape(shape) # pylint: disable=no-member
    indices = indices.diagonal(dim1=-2, dim2=-1)
    result.view(-1)[indices] = diagonal
    return result

def g_term_net(net : torch.nn.Module, dl, relu=False, obj='MSE'):

    Ws = []
    layers = []
    net.eval()

    E_G = None
    E_F = None
    batch_count = 0

    for layer, param in net.named_parameters():
        layers.append(layer[:-7])
        Ws.append(param)
        # print(layer, param.shape)
    
    c = Ws[-1].shape[0]

    ife = IntermediateFeatureExtractor(net, layers)
    for X_b, y_b in iter(dl):
        for X, y in zip(X_b, y_b):
            X = X.unsqueeze(0)
            y = y.unsqueeze(0)
            feats_dict, y_hat = ife(X)

            feats = [feats_dict[l] for l in layers]
            Xs, Cs = [], []
            for feat_in, feat_out in feats:
                Xs.append(feat_in)
                if relu:
                    Cs.append(matrix_diag(feat_out >= 0).float())
            
            if obj == 'MSE':
                Q = matrix_diag(torch.ones_like(feats[-1][-1])).float() * np.sqrt(2) / np.sqrt(c)
            
            Gs = [matrix_diag(torch.ones_like(y)).float()]
            for i in range(len(layers) - 1):
                # print(i, Gs[-1].shape, Ws[-(i + 1)].shape)
                if relu:
                    Gs.append(Gs[-1].matmul(Ws[-(i+1)]).matmul(Cs[-(i+2)]).detach())
                else:
                    Gs.append(Gs[-1].matmul(Ws[-(i+1)]).detach())
            Gs.reverse()

            Fs = []
            for i, layer in enumerate(layers):
                X = Xs[i].unsqueeze(-2)
                QG = Q.matmul(Gs[i])
                F = bkp_2d(QG, X)
                # print(layer, X.shape, QG.shape, F.shape)
                Fs.append(F)
            
            F_concat = torch.cat(Fs, dim=-1)
            G_batch = torch.mean(torch.bmm(torch.transpose(F_concat, -1, -2), F_concat), dim=0)
            F_batch = torch.mean(F_concat, dim=0)
            if E_F is None:
                E_G = G_batch
            else:
                E_G += G_batch
            
            if E_F is None:
                E_F = F_batch
            else:
                E_F += F_batch
            batch_count += 1
    net.train()

    E_G /= batch_count
    E_F /= batch_count

    E_G_diag = torch.zeros_like(E_G)
    d = 0
    for i, layer in enumerate(layers):
        width = Ws[i].view(-1).shape[0]
        E_G_diag[d:d + width, d:d + width] = E_G[d:d + width, d:d + width]
        d = d + width
        
    return (E_G, E_F, E_G_diag)
