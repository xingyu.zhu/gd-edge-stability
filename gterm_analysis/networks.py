import torch.nn as nn

class LinearNet(nn.Module):
    def __init__(self, L, d):
        super(LinearNet, self).__init__()
        self.L = L
        self.layers = []
        for i in range(L):
            self.layers.append(nn.Linear(d, d, bias=False))
            self.add_module("W{}".format(i+1), self.layers[-1])

    def forward(self, x):
        for i in range(self.L):
            x = self.layers[i](x)
        return x

class LinearShrinkNet(nn.Module):
    def __init__(self, L, d):
        super(LinearShrinkNet, self).__init__()
        self.L = L
        self.layers = []
        for i in range(L):
            if i != L - 1:
                self.layers.append(nn.Linear(d + i, d + i + 1, bias=False))
            else:
                self.layers.append(nn.Linear(d + i, d, bias=False))
            self.add_module("W{}".format(i+1), self.layers[-1])

    def forward(self, x):
        for i in range(self.L):
            x = self.layers[i](x)
        return x