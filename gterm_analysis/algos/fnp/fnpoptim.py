import torch
import numpy as np
import itertools
from os.path import join as pathjoin
from copy import deepcopy
from collections import OrderedDict

def sd_add(a: OrderedDict, b: OrderedDict):
    """
    c = a + b
    """
    c = deepcopy(a)
    assert a.keys() == b.keys(), 'params mismatch'
    for opr in c:
        assert c[opr].shape == b[opr].shape, 'shape of {} mismatch'.format(opr)
        if len(c[opr].shape) >= 1: # No long type tensor
            c[opr].add_(b[opr])
    return c


def sd_scalar_mul(a: OrderedDict, b: float):

    c = deepcopy(a)
    for opr in c:
        if len(c[opr].shape) >= 1: # No long type tensor
            c[opr].mul(b)
    return c


def sd_add_(a: OrderedDict, b: OrderedDict):
    """
    Add b to a (inplace)
    """
    assert a.keys() == b.keys(), 'params mismatch'
    for opr in a:
        assert a[opr].shape == b[opr].shape, 'shape of {} mismatch'.format(opr)
        if len(a[opr].shape) >= 1: # No long type tensor
            a[opr].add_(b[opr])
    return a


def sd_scalar_mul_(a: OrderedDict, b: float):

    for opr in a:
        if len(a[opr].shape) >= 1: # No long type tensor
            a[opr].mul(b)
    return a


def normalize_filter_(v, w, sigma):
    """
    Given random direction v and filter weight w, filter-wise normalie v inplace
    """
    v.mul_(sigma * w.norm() / v.norm())


def normalize_filters_(vs, ws, sigma):
    """
    Given random directions vs and filter weights ws, filter-wise normalize vs inplace
    """
    v_norm = torch.norm(vs.view(len(vs), -1), dim=1)
    w_norm = torch.norm(ws.view(len(ws), -1), dim=1) + 1e-10
    scale = w_norm / v_norm * sigma
    for _ in range(len(ws.shape) - 1):
        scale.unsqueeze_(-1)
    vs.mul_(scale)


def normalize_filters_weightbias_(vs_weight, vs_bias, ws_weight, ws_bias, sigma):
    """
    Given random directions vs and filter weights ws, filter-wise normalize vs inplace
    Consider the weight and bias as in the same filter.
    """
    assert len(ws_bias.shape) == 1
    v_norm = torch.sqrt(torch.sum(vs_weight * vs_weight, dim=list(range(1, len(vs_weight.shape)))) + vs_bias * vs_bias) # pylint: disable=maybe-no-member
    w_norm = torch.sqrt(torch.sum(ws_weight * ws_weight, dim=list(range(1, len(ws_weight.shape)))) + ws_bias * ws_bias) # pylint: disable=maybe-no-member
    scale_weight = w_norm / v_norm * sigma
    scale_bias = scale_weight.clone()
    
    for _ in range(len(ws_weight.shape) - 1):
        scale_weight.unsqueeze_(-1)
    vs_weight.mul_(scale_weight)
    vs_bias.mul_(scale_bias)


def gaussian_fn_rand_biassep(sd: OrderedDict, sigma=1.0, bias_noise=True, bias_single_filter=False):
    """
    Create a rescaled random directional "vector" for the neural net.
    The weight for conv layers are rescaled filterwise as decribed in https://arxiv.org/pdf/1712.09913.pdf.
    As proposed in the paper, weight of fc layers and biases should also be rescaled, but they can be disabled in this function.
    We skip bn in this case.
    """

    sd_rand = deepcopy(sd)
    for opr in sd:
        if len(sd[opr].shape) == 0:
            sd_rand[opr] = deepcopy(sd[opr])
        else:
            sd_rand[opr] = torch.zeros_like(sd[opr]) # pylint: disable=maybe-no-member

    for opr in sd:
        # Rescale conv and fc weights (nchw for conv2d)
        if '.weight' in opr:
            assert len(sd[opr].shape) >= 2, 'awkward dimension'
            sd_rand[opr] = torch.empty_like(sd[opr]).normal_(std=1) # pylint: disable=maybe-no-member
            normalize_filters_(sd_rand[opr], sd[opr], sigma)
        
        # Rescale biases
        if '.bias' in opr:
            if bias_noise:
                sd_rand[opr] = torch.empty_like(sd[opr]).normal_(std=1) # pylint: disable=maybe-no-member
                if bias_single_filter:
                    sd_rand[opr].div_(abs(sd_rand[opr]) + 1e-10).mul_(sigma * abs(sd[opr]))
                else:
                    normalize_filter_(sd_rand[opr], sd[opr], sigma)
    return sd_rand


def gaussian_fn_rand_biascombine(sd: OrderedDict, sigma=1.0):
    """
    Create a rescaled random directional "vector" for the neural net.
    The weight for conv layers are rescaled filterwise as decribed in https://arxiv.org/pdf/1712.09913.pdf.
    As proposed in the paper, weight of fc layers and biases should also be rescaled, but they can be disabled in this function.
    We skip bn in this case.
    """

    sd_rand = dict()
    # Rescale conv and fc weights (nchw for conv2d)
    for opr in sd:
        if opr.endswith('.weight'):

            weight_opr = opr
            bias_opr = opr[:-7] + '.bias'
            weight_shape = sd[opr].shape
            assert len(weight_shape) >= 2, 'awkward dimension'
            
            sd_rand[bias_opr] = torch.empty_like(sd[bias_opr]).normal_(std=1) # pylint: disable=maybe-no-member
            sd_rand[weight_opr] = torch.empty_like(sd[weight_opr]).normal_(std=1) # pylint: disable=maybe-no-member
            normalize_filters_weightbias_(sd_rand[weight_opr], sd_rand[bias_opr], sd[weight_opr], sd[bias_opr], sigma)
        
    return sd_rand


def grad_weightbias(param_weight, param_bias, g_weight, g_bias, noise_weight, noise_bias):
    """
    Given random directions vs and filter weights ws, filter-wise normalize vs inplace
    Consider the weight and bias as in the same filter.
    """
    assert len(param_bias.shape) == 1
    sum_dims = list(range(1, len(param_weight.shape)))
    w_norm_sqr = torch.Tensor.sum(param_weight * param_weight, dim=sum_dims) + param_bias * param_bias
    sigma_vTg = torch.Tensor.sum(g_weight * noise_weight, dim=sum_dims) + g_bias * noise_bias # Note that the noise already contains the sigma factor

    scale = sigma_vTg / w_norm_sqr
    scale_weight = scale.clone()
    # print(sigma_vTg.shape, w_norm_sqr.shape, param_weight.shape)
    for _ in sum_dims:
        scale_weight.unsqueeze_(-1)
    g_add_weight = param_weight * scale_weight
    g_add_bias = param_bias * scale
    return g_add_weight, g_add_bias


def grad_weightbias_(param_weight, param_bias, g_weight, g_bias, noise_weight, noise_bias):
    """
    In-place implementation of grad_weightbias
    """
    assert len(param_bias.shape) == 1
    sum_dims = list(range(1, len(param_weight.shape)))
    w_norm_sqr = torch.Tensor.sum(param_weight * param_weight, dim=sum_dims) + param_bias * param_bias
    sigma_vTg = torch.Tensor.sum(g_weight * noise_weight, dim=sum_dims) + g_bias * noise_bias # Note that the noise already contains the sigma factor

    scale = sigma_vTg / w_norm_sqr
    scale_weight = scale.clone()
    # print(sigma_vTg.shape, w_norm_sqr.shape, param_weight.shape)
    for _ in sum_dims:
        scale_weight.unsqueeze_(-1)
    g_weight.data.add_(param_weight * scale_weight)
    g_bias.data.add_(param_bias * scale)


def fnp_grad_biascombine_(param_sd: dict, noise_sd: dict):
    """
    Update the gradient
    """
    # Rescale conv and fc weights (nchw for conv2d)
    for opr in param_sd:
        if opr.endswith('.weight'):
            weight_opr = opr
            bias_opr = opr[:-7] + '.bias'
            grad_weightbias_(param_sd[weight_opr].data,
                             param_sd[bias_opr].data,
                             param_sd[weight_opr].grad,
                             param_sd[bias_opr].grad,
                             noise_sd[weight_opr],
                             noise_sd[bias_opr])


def debug_print_filternorm(m):
    if len(m.shape) == 1:
        print(m)
    else:
        print([float(torch.norm(k)) for k in m])

def get_target_params(net):
    sd = net.state_dict()
    target_params = []
    for name, layer in net.named_modules():
        if isinstance(layer, (torch.nn.Conv2d, torch.nn.Linear, torch.nn.Conv1d, torch.nn.Conv3d)):
            if name + '.weight' in sd and name + '.bias' in sd:
                target_params.append(name + '.weight')
                target_params.append(name + '.bias')
    return target_params

class FNP():

    def __init__(self, net):

        self.net = net
        self.target_params = get_target_params(self.net)
    
    def perturb(self, sigma):
        param_sd = {}
        for param_name, param in self.net.named_parameters():
            if param_name in self.target_params:
                param_sd[param_name] = param
        
        noise_sd = gaussian_fn_rand_biascombine(param_sd, sigma)
        for param_name, param in self.net.named_parameters():
            if param_name in noise_sd:
                param.data += noise_sd[param_name] # shift the parameter to calculate base gradient
        
        return param_sd, noise_sd

    def grad_comp(self, param_sd, noise_sd):
        for param_name, param in self.net.named_parameters():
            if param_name in noise_sd:
                param.data -= noise_sd[param_name] # optimize on the original parameters    
        fnp_grad_biascombine_(param_sd, noise_sd) # update the gradient with remainder terms induced by chain rule
