from .utils import *
import torch
import numpy as np

def approx_fulleigenvec_dict(layers, vxs, vGTAT, sig_GTAT, split_inds, with_bias=False):
    ret = []
    for i, layer in enumerate(layers):
        s, e = split_inds[layer]
        vGTAT_crop = vGTAT[s : e]
        v_kron_x = vGTAT_crop.unsqueeze(-1).matmul(vxs[layer].transpose(-1, -2)).view(-1)
        if with_bias:
            v_kron_x = torch.cat([v_kron_x, vGTAT_crop])
        ret.append(v_kron_x)
        # print(len(v_kron_x))
    vec = torch.cat(ret)
    vec_scale = vec.norm()
    vec.div_(vec.norm())
    return vec.cpu(), sig_GTAT * torch.square(vec_scale)

def approx_fulleigenvec_list(layers, vxs, vGTAT, sig_GTAT, split_inds, with_bias=False):
    ret = []
    for i, layer in enumerate(layers):
        s, e = split_inds[i]
        vGTAT_crop = vGTAT[s : e]
        v_kron_x = vGTAT_crop.unsqueeze(-1).matmul(vxs[i].transpose(-1, -2)).view(-1)
        if with_bias:
            v_kron_x = torch.cat([v_kron_x, vGTAT_crop])
        ret.append(v_kron_x)
        # print(len(v_kron_x))
    vec = torch.cat(ret)
    vec_scale = vec.norm()
    vec.div_(vec.norm())
    return vec.cpu(), sig_GTAT * torch.square(vec_scale)


def linear_kron_approx(net : torch.nn.Module, dl, relu, obj, device):
    Ws = []
    layers = []
    net.eval()

    batch_count = 0

    for layer, param in net.named_parameters():
        if layer.endswith('weight'):
            layers.append(layer[:-7])
            Ws.append(param)
        # print(layer, param.shape)
    
    E_GTAG_full = None
    E_xs = [None for layer in layers]
    E_xxTs = [None for layer in layers]

    c = Ws[-1].shape[0]
    count = 0

    ife = IntermediateFeatureExtractor(net, layers)
    
    for x, y in iter(dl):

        count += x.shape[0]

        feats_dict, y_hat = ife(x)

        feats = [feats_dict[l] for l in layers]
        xs, Cs = [], []

        for i, (feat_in, feat_out) in enumerate(feats):

            if E_xs[i] is None:
                E_xs[i] = torch.sum(feat_in, dim=0)
            else:
                E_xs[i] += torch.sum(feat_in, dim=0)

            x_expand = feat_in.unsqueeze(1)
            xxT = torch.transpose(x_expand, -1, -2).matmul(x_expand)

            if E_xxTs[i] is None:
                E_xxTs[i] = torch.sum(xxT, dim=0)
            else:
                E_xxTs[i] += torch.sum(xxT, dim=0)

            if relu:
                Cs.append(matrix_diag(feat_out >= 0).float())
        
        if obj == 'MSE':
            A = matrix_diag(torch.ones_like(feats[-1][-1])).float() * 2 / c
        
        if relu:
            Gs = [matrix_diag(torch.ones_like(y)).float()]
        else:
            Gs = [matrix_diag(torch.ones(c).to(y.device)).float()]
        for i in range(len(layers) - 1):
            # print(i, Gs[-1].shape, Ws[-(i + 1)].shape)
            if relu:
                Gs.append(Gs[-1].matmul(Ws[-(i+1)]).matmul(Cs[-(i+2)]).detach())
            else:
                Gs.append(Gs[-1].matmul(Ws[-(i+1)]).detach())
        Gs.reverse()

        Fs = []
        G_concat = torch.cat(Gs, dim=-1)
        if not relu:
            A = torch.sum(A, dim=0)
        GTAG_full = torch.transpose(G_concat, -1, -2).matmul(A).matmul(G_concat)
        if relu:
            GTAG_full = torch.sum(GTAG_full, dim=0)
        
        if E_GTAG_full is None:
            E_GTAG_full = GTAG_full
        else:
            E_GTAG_full += GTAG_full
        batch_count += 1

    net.train()
    split_inds = []
    s = 0
    for G in Gs:
        split_inds.append([s, s + G.shape[-1]])
        s += G.shape[-1]

    E_GTAG_full /= count

    for i in range(len(E_xs)):
        E_xs[i] /= count
        E_xs[i].unsqueeze_(-1)
        E_xxTs[i] /= count
    
    GTAT_sigs, GTAT_vecs = eigenthings_tensor_utils(GTAG_full, symmetric=True, topn=1)
    sig_GTAT = GTAT_sigs[0]
    vGTAT = GTAT_vecs[0]

    approx_v_Ex, approx_sig_Ex = approx_fulleigenvec_list(layers, E_xs, vGTAT, sig_GTAT, split_inds)

    # sanity check
    v_ExxTs = []
    for i in range(len(E_xs)):
        vals, vecs = eigenthings_tensor_utils(E_xxTs[i], symmetric=True, topn=5)
        v_ExxTs.append((vecs[0] * torch.sqrt(vals[0])).unsqueeze(-1))
    
    approx_v_ExxT, approx_sig_ExxT = approx_fulleigenvec_list(layers, v_ExxTs, vGTAT, sig_GTAT, split_inds)

    # print(approx_sig_Ex, approx_sig_ExxT)
    
    return approx_sig_Ex, approx_v_Ex.to(device), approx_sig_ExxT, approx_v_ExxT.to(device)
