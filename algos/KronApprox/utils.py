import torch
import numpy as np
import functools
from copy import deepcopy
from collections import OrderedDict

def rgetattr(obj, attr, *args):
    def _getattr(obj, attr):
        return getattr(obj, attr, *args)
    return functools.reduce(_getattr, [obj] + attr.split('.'))

def bkp_2d(t1, t2):
    btsize, t1_h, t1_w = t1.size()
    btsize, t2_h, t2_w = t2.size()
    out_h = t1_h * t2_h
    out_w = t1_w * t2_w

    expanded_t1 = (
        t1.unsqueeze(3)
          .unsqueeze(4)
          .repeat(1, 1, t2_h, t2_w, 1)
          .view(btsize, out_h, out_w)
    )
    for i in range(t1_h):
        for j in range(t1_w):
            expanded_t1[:, i * t2_h: (i + 1) * t2_h, j * t2_w: (j + 1) * t2_w] *= t2
    return expanded_t1

class IntermediateFeatureExtractor:

    def __init__(self, net, target_layers, evaluate=True):
        self.net = net
        self.evaluate = evaluate
        self.target_layers = target_layers
        
    def __call__(self, *args, **kwargs):
        if self.evaluate:
            self.net.eval()
        ret, handles = {}, []
        for var_name in self.target_layers:
            try:
                layer = rgetattr(self.net, var_name)
            except:
                print("var name not found")
            def hook(module, feature_in, feature_out, name=var_name):
                assert name not in ret
                if isinstance(feature_in, tuple):
                    feature_in = feature_in[0]
                if isinstance(feature_out, tuple):
                    feature_out = feature_out[0]
                ret[name] = [feature_in.detach(), feature_out.detach()]
            h = layer.register_forward_hook(hook)
            handles.append(h)
            
        output = self.net(*args, **kwargs).detach()
            
        for h in handles:
            h.remove()
            
        if self.evaluate:
            self.net.train()
        
        return ret, output

def matrix_diag(diagonal):
    N = diagonal.shape[-1]
    shape = diagonal.shape[:-1] + (N, N)
    device, dtype = diagonal.device, diagonal.dtype
    result = torch.zeros(shape, dtype=dtype, device=device) # pylint: disable=no-member
    indices = torch.arange(result.numel(), device=device).reshape(shape) # pylint: disable=no-member
    indices = indices.diagonal(dim1=-2, dim2=-1)
    result.view(-1)[indices] = diagonal
    return result

def eigenthings_tensor_utils(t, device=None, out_device='cpu', symmetric=False, topn=-1):
    """return eigenvals, eigenvecs"""
    t = t.to(device)
    if topn >= 0:
        _, eigenvals, eigenvecs = torch.svd_lowrank(t, q=min(topn, t.size()[0], t.size()[1]))
        eigenvecs = eigenvecs.transpose(0, 1)
    else:
        if symmetric:
            eigenvals, eigenvecs = torch.symeig(t, eigenvectors=True) # pylint: disable=no-member
            eigenvals = eigenvals.flip(0)
            eigenvecs = eigenvecs.transpose(0, 1).flip(0)
        else:
            _, eigenvals, eigenvecs = torch.svd(t, compute_uv=True) # pylint: disable=no-member
            eigenvecs = eigenvecs.transpose(0, 1)
    return eigenvals, eigenvecs
