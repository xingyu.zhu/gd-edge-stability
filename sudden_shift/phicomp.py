import torch
from utils import kp_2d

def comp_phi(Ws):
    """
    Compute the Phi matrix under
    the assumption that the input is whitened (\Sigma_x = I)
    """

    l = len(Ws)
    dev = Ws[0].device
    in_dim = Ws[0].shape[-1]
    out_dim = Ws[-1].shape[0]

    phis = []
    for k in range(l):
        W_prod1 = torch.eye(in_dim).to(dev)
        W_prod2 = torch.eye(out_dim).to(dev)

        # print("k{}".format(k + 1))
        for i in range(k):
            W_prod1 = Ws[i].matmul(W_prod1)
        for i in range(1, l - k):
            W_prod2 = W_prod2.matmul(Ws[-i])
        phi_k = kp_2d(W_prod1, W_prod2.T)
        # print(phi_k.shape)

        phis.append(phi_k.T)
    phi = torch.cat(phis, dim=1)
    # print(phi.shape)

    return phi

