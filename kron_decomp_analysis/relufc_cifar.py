import torch.nn as nn

from networks import ReLUNet_cifar
from algos.KronApprox import *
from utils import *
from dataset import get_cifar

device = 'cuda' if torch.cuda.is_available() else 'cpu'

d = 50
L = 10
n = 50
eta = 2/500
epochs = 10000

with_relu = True

if not with_relu:
    net = ReLUNet_cifar(L, d)
X, Y = get_cifar('mse')


X = X.to(device)
Y = Y.to(device)
dataloader = FakeDL(X, Y, device)
net = net.to(device)

# Xavier Initialization
for param in net.parameters():
    param.data = torch.normal(torch.zeros_like(param), 1/np.sqrt(d)) # pylint: disable=no-member

criterion = nn.MSELoss()
optimizer = torch.optim.SGD(net.parameters(), lr=eta)

for epoch in range(epochs):  # loop over the dataset multiple times
    
    loss_record = []
    eigenvals_record = []
    eigenvecs_record = []
    
    optimizer.zero_grad()
    
    Y_hat = net(X)
    loss = criterion(Y_hat, Y)
    
    loss.backward()
    optimizer.step()
    
    loss_record.append(loss.item())
    if epoch % 100 == 0:
        approx_sig_Ex, approx_v_Ex, approx_sig_ExxT, approx_v_ExxT = linear_kron_approx(net, dataloader, relu=with_relu, obj='MSE', device=device)
        eigenvals, eigenvecs = compute_eigeninfo_eigenthings(net, dataloader, 1, criterion, device)
        eigenvals_record.append(eigenvals)
        eigenvec = eigenvecs[0]

        print(eigenvec.dot(approx_v_Ex))
        print(eigenvec.dot(approx_v_ExxT))
        print(eigenvals, approx_sig_Ex, approx_sig_ExxT)

        for i in range(0, d * L, d):
            vbase_seg = eigenvec[i: i + d]
            vapprox_seg = approx_v_Ex[i: i + d]

            vbase_seg.div_(vbase_seg.norm())
            vapprox_seg.div_(vapprox_seg.norm())

            print(vbase_seg.dot(vapprox_seg))


    if epoch % 100 == 0:
        print("Epoch {} \t Loss: {:.6g} \t Sharpness: {:.6g}".format(epoch, loss.item(), eigenvals_record[-1][0] if len(eigenvals_record) > 0 else -1))

