import torch
import numpy as np
import algos.pyhessian as pyhessian
import algos.hessian_eigenthings as eigenthings

class FakeDL():

    def __init__(self, X, Y, device):
        self.device = device
        self.batchsize = len(X)
        self.X = X.to(device)
        self.Y = Y.to(device)
        self.active = True
        return
    
    def __len__(self):
        return 1
    
    def __next__(self):
        if not self.active:
            raise StopIteration
        self.active = False
        return [self.X, self.Y]
    
    def __iter__(self):
        self.active = True
        return self

    next = __next__

def trace_overlap(s1, s2, device=None):
    M = s1.to(device)
    V = s2.to(device)
    Mt = torch.transpose(M, 0, 1) # pylint: disable=no-member
    n = M.size()[0]
    k = 0
    for i in range(n):
        vi = V[i]
        li = Mt.mv(M.mv(vi))
        ki = torch.dot(li, li) # pylint: disable=no-member
        k += ki
    del Mt, M, V
    return float((k / n).cpu().numpy())

def eigenspace_stability(eigenvecs_record):

    n_vecs = len(eigenvecs_record[0])
    steps = len(eigenvecs_record)
    eigenvecs = [[] for i in range(steps)]
    for i in range(steps):
        for j in range(n_vecs):
            v = torch.cat(eigenvecs_record[i][j]).view(-1)
            eigenvecs[i].append(v)
    res = np.zeros([n_vecs, steps])
    for i in range(1, steps):
        # top j eigenspace
        for j in range(n_vecs):
            old_space = torch.stack([eigenvecs[i-1][k] for k in range(j + 1)])
            new_space = torch.stack([eigenvecs[i][k] for k in range(j + 1)])
            ovlp = trace_overlap(old_space, new_space)
            res[j, i] = ovlp
    return res

def compute_eigeninfo(net, dl, topn, criterion, device):
    # Computing the top n eigenvectors using Power Iteration Method.
    net.eval()
    hessian_comp = pyhessian.hessian(net, criterion, dataloader=dl, cuda=False if device=="cpu" else True)
    eigenvals, eigenvecs = hessian_comp.eigenvalues(top_n=topn, tol=1e-4, maxIter=1000)
    # print(eigenvals)
    net.train()
    return eigenvals, eigenvecs

def compute_eigeninfo_eigenthings(net, dl, topn, criterion, device):
    # Computing the top n eigenvectors using Power Iteration Method.
    net.eval()
    eigenvals, eigenvecs = eigenthings.compute_hessian_eigenthings(net, dl, criterion, topn, mode='power_iter', max_samples=1024)
    # print(eigenvals)
    net.train()
    eigenvecs = [torch.from_numpy(eigenvec).to(device) for eigenvec in eigenvecs]
    return eigenvals, eigenvecs