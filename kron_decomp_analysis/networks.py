import torch.nn as nn
import torch.nn.functional as F

class LinearNet(nn.Module):
    def __init__(self, L, d):
        super().__init__()
        self.L = L
        self.layers = []
        for i in range(L):
            self.layers.append(nn.Linear(d, d, bias=False))
            self.add_module("W{}".format(i+1), self.layers[-1])

    def forward(self, x):
        for i in range(self.L):
            x = self.layers[i](x)
        return x

class ReLUNet(nn.Module):
    def __init__(self, L, d):
        super().__init__()
        self.L = L
        self.layers = []
        for i in range(L):
            self.layers.append(nn.Linear(d, d, bias=False))
            self.add_module("W{}".format(i+1), self.layers[-1])

    def forward(self, x):
        for i in range(self.L):
            x = self.layers[i](x)
            if i != self.L - 1:
                x = F.relu(x)
        return x

class ReLUNet_cifar(nn.Module):
    def __init__(self, L, d, input_dim=32*32*3, output_dim=10):
        super().__init__()
        self.L = L
        self.layers = []
        assert L >= 2
        for i in range(L):

            if i == 0:
                layer = nn.Linear(input_dim, d, bias=False)
            elif i == L - 1:
                layer = nn.Linear(d, output_dim, bias=False)
            else:   
                layer = nn.Linear(d, d, bias=False)
            self.layers.append(layer)
            self.add_module("W{}".format(i+1), self.layers[-1])

    def forward(self, x):
        x = torch.flatten(x)
        for i in range(self.L):
            x = self.layers[i](x)
            if i != self.L - 1:
                x = F.relu(x)
        return x