import numpy as np
import torch
import torch.nn as nn
from utils import FakeDL, eigenspace_stability, prepare_net
import algos.pyhessian as pyhessian 
import algos.hessian_eigenthings as hessian_eigenthings
from collections import OrderedDict
from vis import *
from datetime import datetime


device = 'cpu' if not torch.cuda.is_available() else 'cuda'

def get_dataset(n, d):
    assert n >= d
    X = np.sqrt(n) * torch.qr(torch.normal(torch.zeros(n, d)))[0] # pylint: disable=no-member
    A = torch.normal(torch.zeros(d, d)) # pylint: disable=no-member
    Y = torch.matmul(X, A) # pylint: disable=no-member
    return X, Y


class LinearNet(nn.Module):
    def __init__(self, L, d):
        super(LinearNet, self).__init__()
        self.L = L
        self.layers = []
        for i in range(L):
            self.layers.append(nn.Linear(d, d, bias=False))
            self.add_module("W{}".format(i+1), self.layers[-1])

    def forward(self, x):
        for i in range(self.L):
            x = self.layers[i](x)
        return x


def compute_eigeninfo(net, dl, topn, criterion):
    # Computing the top n eigenvectors using Power Iteration Method.
    net.eval()
    hessian_comp = pyhessian.hessian(net, criterion, dataloader=dl, cuda=True)
    eigenvals, eigenvecs = hessian_comp.eigenvalues(top_n=topn, tol=1e-4, maxIter=1000)
    # print(eigenvals)
    net.train()
    return eigenvals, eigenvecs

def eigenthings_calc_layer(net, dl, topn_eigenthings, criterion, eigenval_criteria='LM', eigenthings_mode="power_iter", layer_method='block', max_samples_count=1024):
    assert eigenthings_mode in ["lanczos", "power_iter"]
    results = []
    start = 0
    layerinfo_dict = OrderedDict()
    eigenval_dict = OrderedDict()
    eigenvec_dict = OrderedDict()
    layer_num = 0
    for _ in net.named_parameters():
        layer_num += 1

    for i, (var_name, var_param) in enumerate(net.named_parameters()):
        start_time_layer = datetime.now()
        # print("\nComputing Hessian Layer Block {} Top {} Eigenthings. {} / {}".format(var_name, topn_eigenthings, i + 1, layer_num))
        end = start + var_param.view(-1).size()[0]
        layerinfo_dict[var_name] = (start, end)
        eigennum = min(end - start, topn_eigenthings)
        eigenvals, eigenvecs = hessian_eigenthings.compute_hessian_eigenthings_layer(net, dl, criterion, eigennum, mode=eigenthings_mode, max_samples=max_samples_count, start=start, end=end, method=layer_method)
        if len(eigenvals) > 1:
            if eigenvals[1] > eigenvals[0]:
                eigenvals = eigenvals[::-1]
                eigenvecs = eigenvecs[::-1]
        # print(eigenvals[:10])
        # print(eigenvecs.shape)
        eigenval_dict[var_name] = eigenvals
        eigenvec_dict[var_name] = eigenvecs
        start = end
    end_time = datetime.now()
    return layerinfo_dict, eigenval_dict, eigenvec_dict


def compute_lw_eigeninfo(net, dl, topn, criterion):
    net.eval()
    res = eigenthings_calc_layer(net, dl, topn, criterion)
    net.train()
    return res[1], res[2]


def train(X, Y, L, d, eta, epochs, hessian_eval_epochs, hessian_topn, save_eigenvecs=True):
    """
    X, Y : dataset
    L, d : as defined in the paper
    epochs : total number of epochs to train
    hessian_eval_epochs : list of integers indicating on which epochs to calculate the eigeninformation
    hessian_topn : number of top eigenvector/values to compute
    save_eigenvecs : whether to save the eigenvectors
    """
    print("d{}_e{}_L{}".format(d, epochs, L))

    # Dataloader for Hessian Calculation
    net = LinearNet(L, d)

    net, device = prepare_net(net)
    dataloader = FakeDL(X, Y, device)
    loss_record = []
    eigenvals_record = []
    lw_eigenvals_record = []
    eigenvecs_record = []
    lw_eigenvecs_record = []

    # Xavier Initialization
    for param in net.parameters():
        param.data = torch.normal(torch.zeros_like(param), 1/np.sqrt(d)) # pylint: disable=no-member

    criterion = nn.MSELoss()
    optimizer = torch.optim.SGD(net.parameters(), lr=eta)

    for epoch in range(epochs):  # loop over the dataset multiple times
        optimizer.zero_grad()
        
        Y_hat = net(X)
        loss = criterion(Y_hat, Y)
        
        loss.backward()
        optimizer.step()
        
        loss_record.append(loss.item())

        if epoch in hessian_eval_epochs:
            # print("Computing eigen")
            eigenvals, eigenvecs = compute_eigeninfo(net, dataloader, hessian_topn, criterion)
            eigenvals_record.append(eigenvals)
            if save_eigenvecs:
                eigenvecs_record.append(eigenvecs)
            # print("Computing lw eigen")
            lw_eigenvals, lw_eigenvecs = compute_lw_eigeninfo(net, dataloader, hessian_topn, criterion)
            lw_eigenvals_record.append(list(lw_eigenvals.values()))
            if save_eigenvecs:
                lw_eigenvecs_record.append(list(lw_eigenvecs.values()))

            # print(lw_eigenvals, lw_eigenvecs)

        if epoch % 200 == 0:
            print("Epoch {} \t Loss: {:.6g} \t Sharpness: {:.6g}".format(epoch, loss.item(), eigenvals_record[-1][0] if len(eigenvals_record) > 0 else -1))

    return loss_record, eigenvals_record, eigenvecs_record, lw_eigenvals_record, lw_eigenvecs_record



def main():
    # Configs, current set as proposed in the paper
    d = 50
    n = 50
    L = int(sys.argv[1])

    X, Y = get_dataset(n, d)
    X, Y = X.to(device), Y.to(device)

    eta = 2/1000
    epochs = int(sys.argv[2])
    hessian_eval_epochs = list(range(0, epochs, 200))

    train_result = train(X, Y, L, d, eta, epochs, hessian_eval_epochs, hessian_topn=1, save_eigenvecs=False)
    loss_record, eigenvals_record, eigenvecs_record, lw_eigenvals_record, lw_eigenvecs_record = train_result

    visualize_base(loss_record, hessian_eval_epochs, eigenvals_record, eigenvecs_record, eta, d, n, L)
    visualize_lw(loss_record, hessian_eval_epochs, lw_eigenvals_record, lw_eigenvecs_record, eta, d, n, L)

if __name__ == "__main__":
    main()
