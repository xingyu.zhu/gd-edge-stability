import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import os, sys
from utils import eigenspace_stability

mpl.use("Agg")

def get_image_path(name):
    vis_dir = "/usr/xtmp/CSPlus/VOLDNN/Shared/Visualizations/"
    img_path = os.path.join(vis_dir, "{}.jpg".format(name))
    print("https://users.cs.duke.edu/~xz231" + img_path.split("Shared")[1] + '\n')
    return img_path

def visualize_base(loss_record, hessian_eval_epochs, eigenvals_record, eigenvecs_record, eta, d, n, L, tag=None):

    if tag is None:
        title = "d{}_n{}_L{}".format(d, n, L)
    else:
        title = "d{}_n{}_L{}_{}".format(d, n, L, tag)
    # Plot the empirical loss
    plt.figure(figsize=(20, 6))
    plt.suptitle(title)
    plt.subplot(121)
    plt.title(r"Training Loss $\eta$={:.4g}".format(eta))
    step = len(loss_record) // 1000 if len(loss_record) > 1000 else 1
    plt.plot(np.arange(0, len(loss_record), step), np.array(loss_record)[np.arange(0, len(loss_record), step).astype(int)], label=r'$\eta$={:.4g}'.format(eta))
    plt.xlabel('Epochs')

    # Plot the eigenvalues
    plt.subplot(122)
    if np.max(eigenvals_record[0]) > 1/eta:
        plt.axhline(y=2/eta, linestyle=':')
    plt.title(r"Top eigenvalues $\eta$={:.4g}".format(eta))
    for k in range(len(eigenvals_record[0])):
        plt.scatter(hessian_eval_epochs, [v[k] for v in eigenvals_record], label=r'$\lambda_{}$'.format(k+1))
    plt.legend()
    plt.xlabel('Epochs')
    fig_name = '{}_eta{:.4g}_epochs{}_eigenvals'.format(title, eta, len(loss_record))
    fig_name = get_image_path(fig_name)
    plt.savefig(fig_name)

    if len(eigenvecs_record) != 0:
        eigenspace_ovlp = eigenspace_stability(eigenvecs_record)
        plt.figure(figsize=(8, 6))
        plt.suptitle(title)
        plt.title(r"Eigenspace Overlap $\eta$={:.4g} Sample rate {}".format(eta, len(loss_record) / len(eigenvals_record)))
        for k in range(len(eigenspace_ovlp)):
            plt.scatter(hessian_eval_epochs, eigenspace_ovlp[k], label='top {} eigenspace'.format(k+1))
        plt.xlabel('Epochs')
        plt.legend()
        fig_name = '{}_eta{:.4g}_epochs{}_eigenvecs'.format(title, eta, len(loss_record))
        fig_name = get_image_path(fig_name)
        plt.savefig(fig_name)


def visualize_lw(loss_record, hessian_eval_epochs, lw_eigenvals_record, lw_eigenvecs_record, eta, d, n, L):

    title = "LW_d{}_n{}_L{}".format(d, n, L)
    # Plot the empirical loss
    plt.figure(figsize=(12, 6))
    plt.suptitle(title)
    plt.subplot(121)
    plt.title(r"Training Loss $\eta$={:.4g}".format(eta))
    step = len(loss_record) // 1000 if len(loss_record) > 1000 else 1
    plt.plot(np.arange(0, len(loss_record), step), np.array(loss_record)[np.arange(0, len(loss_record), step).astype(int)], label=r'$\eta$={:.4g}'.format(eta))
    plt.yscale('log')
    plt.xlabel('Epochs')

    # Plot the eigenvalues
    cmap = mpl.cm.get_cmap('plasma')
    plt.subplot(122)
    plt.title(r"Top eigenvalues $\eta$={:.4g}".format(eta))
    for k in range(len(lw_eigenvals_record[0])):
        c = cmap(k / len(lw_eigenvals_record[0]))
        plt.scatter(hessian_eval_epochs, [lw_eigenvals_record[i][k] for i in range(len(hessian_eval_epochs))], label='Layer{}'.format(k+1), color=c)
    plt.legend()
    plt.xlabel('Epochs')
    fig_name = '{}_eta{:.4g}_epochs{}_eigenvals'.format(title, eta, len(loss_record))
    fig_name = get_image_path(fig_name)
    plt.savefig(fig_name)
