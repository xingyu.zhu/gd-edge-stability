import numpy as np
import torch
import torch.nn as nn
from utils import *
import algos.pyhessian as pyhessian 
import torch.nn.functional as F

from vis import *


device = 'cpu' if not torch.cuda.is_available() else 'cuda'

def get_dataset(n, d):
    assert n >= d
    torch.random.manual_seed(0)
    X = np.sqrt(n) * torch.qr(torch.normal(torch.zeros(n, d)))[0] # pylint: disable=no-member
    A = torch.normal(torch.zeros(d, d)) # pylint: disable=no-member
    # A = torch.eye(d) # pylint: disable=no-member
    Y = torch.matmul(X, A) # pylint: disable=no-member
    return X, Y


class LinearNet(nn.Module):
    def __init__(self, L, d):
        super(LinearNet, self).__init__()
        self.L = L
        self.layers = []
        for i in range(L):
            self.layers.append(nn.Linear(d, d, bias=False))
            self.add_module("W{}".format(i+1), self.layers[-1])

    def forward(self, x):
        for i in range(self.L):
            x = self.layers[i](x)
        return x

def compute_eigeninfo(net, dl, topn, criterion):
    # Computing the top n eigenvectors using Power Iteration Method.
    net.eval()
    hessian_comp = pyhessian.hessian(net, criterion, dataloader=dl, cuda=True)
    eigenvals, eigenvecs = hessian_comp.eigenvalues(top_n=topn, tol=1e-6, maxIter=1000)
    # print(eigenvals)
    net.train()
    return eigenvals, eigenvecs


def train(X, Y, L, d, eta, epochs, hessian_eval_epochs, hessian_topn, save_eigenvecs=True):
    """
    X, Y : dataset
    L, d : as defined in the paper
    epochs : total number of epochs to train
    hessian_eval_epochs : list of integers indicating on which epochs to calculate the eigeninformation
    hessian_topn : number of top eigenvector/values to compute
    save_eigenvecs : whether to save the eigenvectors
    """
    print("d{}_e{}_L{}_topn{}".format(d, epochs, L, hessian_topn))

    # Dataloader for Hessian Calculation
    net = LinearNet(L, d)

    net, device = prepare_net(net)
    dataloader = FakeDL(X, Y, device)
    loss_record = []
    eigenvals_record = []
    eigenvecs_record = []

    # Xavier Initialization
    for param in net.parameters():
        param.data = torch.normal(torch.zeros_like(param), 1/np.sqrt(d)) # pylint: disable=no-member

    criterion = nn.MSELoss()
    optimizer = torch.optim.SGD(net.parameters(), lr=eta)
    init_net_vec = None
    grad_init = None
    net_dists_record = []
    grad_dot_record = [0]

    for epoch in range(epochs):  # loop over the dataset multiple times
        optimizer.zero_grad()
        
        Y_hat = net(X)
        loss = criterion(Y_hat, Y)
        
        loss.backward()
        optimizer.step()

        loss_record.append(loss.item())

        if epoch in hessian_eval_epochs:
            eigenvals, eigenvecs = compute_eigeninfo(net, dataloader, hessian_topn, criterion)
            eigenvecs = [torch.cat([w.view(-1) for w in eigenvecs[i]]) for i in range(len(eigenvecs))]
            eigenvals_record.append(eigenvals)
            print(eigenvals)
            if save_eigenvecs:
                eigenvecs_record.append(eigenvecs)
            
            net_vec = vectorize_net(net)
            if init_net_vec is None:
                init_net_vec = vectorize_net(net)
            else:
                grad = (net_vec_prev - net_vec) * eta
                grad /= torch.norm(grad)
                if grad_init is None:
                    grad_init = grad.clone()
                grad_dot_record.append(torch.dot(grad, grad_init).cpu().item())
                print([torch.dot(grad, eigenvec) for eigenvec in eigenvecs])
            net_vec_prev = net_vec
            net_dists_record.append(torch.norm(net_vec - init_net_vec).cpu().item())

        if epoch % 500 == 0 or epoch in hessian_eval_epochs:
            print("Epoch {} \t Loss: {:.6g} \t Sharpness: {:.6g}".format(epoch, loss.item(), eigenvals_record[-1][0] if len(eigenvals_record) > 0 else -1))
    
    misc = [net_dists_record, grad_dot_record]

    return loss_record, eigenvals_record, eigenvecs_record, misc

def visualize_net_dist(hessian_eval_epochs, eigenvals_record, dist_record, grad_record, eta, d, n, L, tag=None):

    if tag is None:
        title = "Dist_d{}_n{}_L{}".format(d, n, L)
    else:
        title = "Dist_d{}_n{}_L{}_{}".format(d, n, L, tag)
    # Plot the empirical loss
    plt.figure(figsize=(20, 6))
    plt.suptitle(title)
    # Plot the eigenvalues
    plt.subplot(131)
    if np.max(eigenvals_record[0]) > 1/eta:
        plt.axhline(y=2/eta, linestyle=':')
    plt.title(r"Top eigenvalues $\eta$={:.4g}".format(eta))
    for k in range(len(eigenvals_record[0])):
        plt.scatter(hessian_eval_epochs, [v[k] for v in eigenvals_record], label=r'$\lambda_{}$'.format(k+1))
    plt.legend()
    plt.xlabel('Epochs')

        # Plot the eigenvalues
    plt.subplot(132)
    plt.title("Distance between models")
    plt.scatter(hessian_eval_epochs, dist_record)
    plt.xlabel('Epochs')

    plt.subplot(133)
    plt.title("Dot product of gradient")
    plt.scatter(hessian_eval_epochs[1:], grad_record[1:])
    plt.xlabel('Epochs')

    fig_name = '{}_eta{:.4g}_epochs{}-{}_eigenvals'.format(title, eta, np.min(hessian_eval_epochs), np.max(hessian_eval_epochs))
    fig_name = get_image_path(fig_name)
    plt.savefig(fig_name)

def main():
    # Configs, current set as proposed in the paper
    d = 50
    n = 50
    L = int(sys.argv[1])

    X, Y = get_dataset(n, d)
    X, Y = X.to(device), Y.to(device)

    eta = 2/500
    epochs = int(sys.argv[2])
    hessian_eval_epochs = list(range(epochs-10, epochs, 1))

    train_result = train(X, Y, L, d, eta, epochs, hessian_eval_epochs, hessian_topn=2, save_eigenvecs=False)
    loss_record, eigenvals_record, eigenvecs_record, misc = train_result

    net_dists_record, grad_dot_record = misc
    print(net_dists_record, grad_dot_record)

    visualize_base(loss_record, hessian_eval_epochs, eigenvals_record, eigenvecs_record, eta, d, n, L)
    # visualize_net_dist(hessian_eval_epochs, eigenvals_record, net_dists_record, eta, d, n, L)
    visualize_net_dist(hessian_eval_epochs, eigenvals_record, net_dists_record, grad_dot_record, eta, d, n, L)

if __name__ == "__main__":
    main()
