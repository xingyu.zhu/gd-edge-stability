import numpy as np
import torch
import torch.nn as nn
from utils import FakeDL, eigenspace_stability, prepare_net
import algos.pyhessian as pyhessian 
import torch.nn.functional as F

from vis import *


device = 'cpu' if not torch.cuda.is_available() else 'cuda'

def get_dataset(n, d):
    assert n >= d
    X = np.sqrt(n) * torch.qr(torch.normal(torch.zeros(n, d)))[0] # pylint: disable=no-member
    A = torch.normal(torch.zeros(d, d)) # pylint: disable=no-member
    # A = torch.eye(d) # pylint: disable=no-member
    Y = torch.matmul(X, A) # pylint: disable=no-member
    return X, Y


class LinearNet(nn.Module):
    def __init__(self, L, d):
        super(LinearNet, self).__init__()
        self.L = L
        self.layers = []
        for i in range(L):
            self.layers.append(nn.Linear(d, d, bias=False))
            self.add_module("W{}".format(i+1), self.layers[-1])

    def forward(self, x):
        for i in range(self.L):
            x = self.layers[i](x)
        return x

class ReLUFC(nn.Module):

    def __init__(self, L, d):
        super(ReLUFC, self).__init__()
        self.L = L
        self.layers = []
        for i in range(L):
            self.layers.append(nn.Linear(d, d, bias=False))
            self.add_module("W{}".format(i+1), self.layers[-1])

    def forward(self, x):
        for i in range(self.L - 1):
            x = F.relu(self.layers[i](x))
        x = self.layers[-1](x)
        return x

def compute_eigeninfo(net, dl, topn, criterion):
    # Computing the top n eigenvectors using Power Iteration Method.
    net.eval()
    hessian_comp = pyhessian.hessian(net, criterion, dataloader=dl, cuda=True)
    eigenvals, eigenvecs = hessian_comp.eigenvalues(top_n=topn, tol=1e-6, maxIter=1000)
    # print(eigenvals)
    net.train()
    return eigenvals, eigenvecs


def train(X, Y, L, d, eta, epochs, hessian_eval_epochs, hessian_topn, save_eigenvecs=True):
    """
    X, Y : dataset
    L, d : as defined in the paper
    epochs : total number of epochs to train
    hessian_eval_epochs : list of integers indicating on which epochs to calculate the eigeninformation
    hessian_topn : number of top eigenvector/values to compute
    save_eigenvecs : whether to save the eigenvectors
    """
    print("Relu_d{}_e{}_L{}_topn{}".format(d, epochs, L, hessian_topn))

    # Dataloader for Hessian Calculation
    net = ReLUFC(L, d)

    net, device = prepare_net(net)
    dataloader = FakeDL(X, Y, device)
    loss_record = []
    eigenvals_record = []
    eigenvecs_record = []

    # Xavier Initialization
    for param in net.parameters():
        param.data = torch.normal(torch.zeros_like(param), 1/np.sqrt(d)) # pylint: disable=no-member

    criterion = nn.MSELoss()
    optimizer = torch.optim.SGD(net.parameters(), lr=eta)

    for epoch in range(epochs):  # loop over the dataset multiple times
        optimizer.zero_grad()
        
        Y_hat = net(X)
        loss = criterion(Y_hat, Y)
        
        loss.backward()
        optimizer.step()
        
        loss_record.append(loss.item())

        if epoch in hessian_eval_epochs:
            eigenvals, eigenvecs = compute_eigeninfo(net, dataloader, hessian_topn, criterion)
            eigenvals_record.append(eigenvals)
            print(eigenvals)
            if save_eigenvecs:
                eigenvecs_record.append(eigenvecs)

        if epoch % 1 == 0:
            print("Epoch {} \t Loss: {:.6g} \t Sharpness: {:.6g}".format(epoch, loss.item(), eigenvals_record[-1][0] if len(eigenvals_record) > 0 else -1))

    return loss_record, eigenvals_record, eigenvecs_record



def main():
    # Configs, current set as proposed in the paper
    d = 50
    n = 50
    L = int(sys.argv[1])

    X, Y = get_dataset(n, d)
    X, Y = X.to(device), Y.to(device)

    eta = 2/1000
    epochs = int(sys.argv[2])
    hessian_eval_epochs = list(range(0, epochs, 1))

    train_result = train(X, Y, L, d, eta, epochs, hessian_eval_epochs, hessian_topn=2, save_eigenvecs=False)
    loss_record, eigenvals_record, eigenvecs_record = train_result

    visualize_base(loss_record, hessian_eval_epochs, eigenvals_record, eigenvecs_record, eta, d, n, L, 'RELU')

if __name__ == "__main__":
    main()
